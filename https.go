package goauth

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

func Post(url string, body io.Reader, headers map[string]string) ([]byte, error) {
	client := &http.Client{}
	request, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return nil, err
	}
	for k, v := range headers {
		request.Header.Add(k, v)
	}
	res, err := client.Do(request)
	if err != nil {
		fmt.Println("err:", err.Error())
		return nil, err
	}
	b, err := ioutil.ReadAll(res.Body)
	return b, err
}

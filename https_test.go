package goauth

import "testing"

func TestPost(t *testing.T) {
	headers := map[string]string{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODYzMjQ2NzUsImlhdCI6MTY4MTE0MDY3NSwidXNlcl9pZCI6MTAwMDIsInVzZXJuYW1lIjoi5b6u5L-h55So5oi3IiwidXNlclR5cGUiOjIsImRldmljZUlkIjoiIn0.0QnwwYV0qLMkI4cKlv7arkJG75z53gxuPcc7mPzS788"}
	post, err := Post("http://localhost:8066/api/user/info", nil, headers)
	if err != nil {
		return
	}

	t.Log(string(post))
}
